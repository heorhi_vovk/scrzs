package by.intexsoft.scrzs.repository;

import by.intexsoft.scrzs.entity.User;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import org.cassandraunit.spring.CassandraDataSet;
import org.cassandraunit.spring.CassandraUnitTestExecutionListener;
import org.cassandraunit.spring.EmbeddedCassandra;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({CassandraUnitTestExecutionListener.class})
@CassandraDataSet(value = "cql/cassCqlWithoutKeyspace.cql", keyspace = "mykeyspace")
@EmbeddedCassandra()
public class CassandraTest {

    private final String cassHost = "127.0.0.1";
    private final int cassPort = 9142;

    @Test
    public void gettingUserByIdQueryTest() {
        Cluster cluster = Cluster.builder().addContactPoints(cassHost).withPort(cassPort).build();
        Session session = cluster.connect("mykeyspace");
        ResultSet result = session.execute("select * from user WHERE userid = 99050");
        Assert.assertTrue(result.iterator().next().getString("firstname").equals("Ted"));
    }

    @Test
    public void updateUserQueryTest() {
        Cluster cluster = Cluster.builder().addContactPoints(cassHost).withPort(cassPort).build();
        Session session = cluster.connect("mykeyspace");
        User user = new User(88066666L, "userToUpdate2", "firstn2ameeee", "lastna2meeee", 323, "+9999929999");
        String updateQuery = "UPDATE mykeyspace.user SET username='" + user.getUsername() +
                "', firstname='" + user.getFirstname() +
                "', lastname='" + user.getLastname() +
                "', age=" + user.getAge() +
                ", phonenumber='" + user.getPhonenumber() +
                "' WHERE userid=" + user.getUserid();
        session.execute(updateQuery);
        ResultSet result = session.execute("select * from user WHERE userid = 88066666");
        Assert.assertTrue(result.iterator().next().getString("firstname").equals("firstn2ameeee"));
    }

    @Test
    public void deleteUserByIdQueryTest() {
        Cluster cluster = Cluster.builder().addContactPoints(cassHost).withPort(cassPort).build();
        Session session = cluster.connect("mykeyspace");
        String query = "DELETE FROM mykeyspace.user WHERE userid = 99050";
        session.execute(query);
        ResultSet result = session.execute("select * from user WHERE userid = 99050");
        Assert.assertFalse(result.iterator().hasNext());
    }
}


