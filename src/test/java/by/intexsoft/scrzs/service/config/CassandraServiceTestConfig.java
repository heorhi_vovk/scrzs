package by.intexsoft.scrzs.service.config;

import by.intexsoft.scrzs.repository.CassandraRepository;
import by.intexsoft.scrzs.service.CassandraUserService;
import by.intexsoft.scrzs.service.ZKManager;
import by.intexsoft.scrzs.service.impl.CassandraUserServiceImpl;
import by.intexsoft.scrzs.service.impl.ZKManagerImpl;
import org.apache.spark.api.java.JavaSparkContext;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:appTest.properties")
public class CassandraServiceTestConfig {

    @Bean
    public JavaSparkContext javaSparkContext() {
        return Mockito.mock(JavaSparkContext.class);
    }

    @Bean
    public CassandraRepository cassandraRepository() {
        return Mockito.mock(CassandraRepository.class);
    }

    @Bean
    public ZKManager zkManager() {
        return Mockito.mock(ZKManagerImpl.class);
    }

    @Bean
    public CassandraUserService cassandraService() {
        return new CassandraUserServiceImpl(javaSparkContext(), zkManager(), cassandraRepository());
    }
}
