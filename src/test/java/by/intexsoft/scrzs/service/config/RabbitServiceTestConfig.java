package by.intexsoft.scrzs.service.config;

import by.intexsoft.scrzs.service.RabbitService;
import org.mockito.Mockito;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:appTest.properties")
public class RabbitServiceTestConfig {

    @Bean
    public RabbitTemplate rabbitTemplate() {
        return Mockito.mock(RabbitTemplate.class);
    }

    @Bean
    public RabbitService rabbitService() {
        return new RabbitService(rabbitTemplate());
    }
}
