package by.intexsoft.scrzs.service.config;

import by.intexsoft.scrzs.service.CassandraUserService;
import by.intexsoft.scrzs.service.RabbitQueuesListener;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:appTest.properties")
public class RabbitListenerTestConfig {

    @Bean
    public RabbitQueuesListener rabbitQueuesListener() {
        return new RabbitQueuesListener(cassandraUserService());
    }

    @Bean
    public CassandraUserService cassandraUserService() {
        return Mockito.mock(CassandraUserService.class);
    }

}
