package by.intexsoft.scrzs.service.config;

import by.intexsoft.scrzs.service.ZKManager;
import by.intexsoft.scrzs.service.impl.ZKManagerImpl;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.imps.CuratorFrameworkImpl;
import org.apache.zookeeper.ZooKeeper;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:appTest.properties")
public class ZkManagerTestConfig {

    @Bean
    public CuratorFramework curatorFramework() {
        return Mockito.mock(CuratorFrameworkImpl.class);
    }

    @Bean
    public ZKManager zkManager() {
        return new ZKManagerImpl(curatorFramework());
    }

    @Bean
    public ZooKeeper zookeeper() {
        return Mockito.mock(ZooKeeper.class);
    }

}