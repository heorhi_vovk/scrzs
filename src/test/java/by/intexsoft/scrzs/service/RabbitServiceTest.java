package by.intexsoft.scrzs.service;

import by.intexsoft.scrzs.service.config.RabbitServiceTestConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RabbitServiceTestConfig.class})
public class RabbitServiceTest {


    @Value("${rabbitmq.messagesExchange}")
    private String exchange;

    @Autowired
    private RabbitService rabbitService;

    @Autowired
    private RabbitTemplate rabbitTemplate;


    @Test
    public void testSendToExchange() {
        rabbitService.sendToExchange(3729874298L, "read");
        verify(rabbitTemplate, times(1)).convertAndSend(exchange, "read", 3729874298L);
    }
}
