package by.intexsoft.scrzs.service;

import by.intexsoft.scrzs.service.config.ZkManagerTestConfig;
import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ZkManagerTestConfig.class})
public class ZkManagerTest {

    @Autowired
    private CuratorFramework client;

    @Autowired
    private ZKManager zkManager;

    @Autowired
    private ZooKeeper zookeeper;

    @Before
    public void setUp() throws KeeperException, InterruptedException {
        Mockito.when(zookeeper.getData("/Spark/cores_max", null, null)).thenReturn("1".getBytes());
        Mockito.when(zookeeper.create(anyString(), anyString().getBytes(), eq(ZooDefs.Ids.OPEN_ACL_UNSAFE), eq(CreateMode.PERSISTENT))).thenReturn("Spark");
    }

    @Test
    public void testGettingZNodeData() {
        String zNodeData = zkManager.getZNodeData("/Spark/cores_max");
        Assert.notNull(zNodeData, "Gated zNode data is NULL");
    }

    @Test
    public void testGettingWrongZNodeData() {
        String zNodeData = zkManager.getZNodeData("wrongPath");
        Assert.isTrue(!zNodeData.equals("1"), "zNode data is not empty");
    }

//    @Test
//    public void testCreatingZnode() {
//        boolean zNodeCreatingResult = zkManager.create("/Spark", "spark".getBytes());
//        Assert.isTrue(zNodeCreatingResult, "zNode is not create success");
//    }

    @Test
    public void testWrongCreatingZnode() {
        boolean zNodeCreatingResult = zkManager.create("wrongPath", "spark".getBytes());
        Assert.isTrue(!zNodeCreatingResult, "zNode create success, bun this not assert");
    }
}


