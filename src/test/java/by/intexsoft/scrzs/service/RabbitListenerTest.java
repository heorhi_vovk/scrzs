package by.intexsoft.scrzs.service;

import by.intexsoft.scrzs.entity.User;
import by.intexsoft.scrzs.service.config.RabbitListenerTestConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RabbitListenerTestConfig.class})
public class RabbitListenerTest {

    @Autowired
    private RabbitQueuesListener rabbitQueuesListener;

    @Autowired
    private CassandraUserService cassandraUserService;

    @Test
    public void testListenCreateQueue() {
        User user = new User(88066666L, "created", "acreatere", "acereter", 32223, "+777777777");
        rabbitQueuesListener.listenCreateQueue(user);
        verify(cassandraUserService, times(1)).saveUser(user);
    }

    @Test
    public void testListenReadQueue() {
        User user = new User();
        user.setUserid(3729874298L);
        rabbitQueuesListener.listenReadQueue(user);
        verify(cassandraUserService, times(1)).getUserById(user.getUserid());
    }

    @Test
    public void testListenUpdateQueue() {
        User user = new User(88066666L, "userToUpdate2", "firstnameeee", "lastnameeee", 323, "+999999999");
        rabbitQueuesListener.listenUpdateQueue(user);
        verify(cassandraUserService, times(1)).updateUser(user);
    }

    @Test
    public void testListenDeleteQueue() {
        User user = new User();
        user.setUserid(88066666L);
        rabbitQueuesListener.listenDeleteQueue(user);
        verify(cassandraUserService, times(1)).deleteUserById(user.getUserid());
    }
}
