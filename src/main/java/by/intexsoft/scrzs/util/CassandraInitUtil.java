package by.intexsoft.scrzs.util;


import by.intexsoft.scrzs.repository.CassandraRepository;
import by.intexsoft.scrzs.service.ZKManager;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Cassandra initialization class
 */
@Service
@Order(-1)
public class CassandraInitUtil {

    private String cqlFilePath;
    private final CassandraRepository cassandraRepository;

    @Autowired
    public CassandraInitUtil(CassandraRepository cassandraRepository, ZKManager zkManager) {
        this.cassandraRepository = cassandraRepository;
        cqlFilePath = zkManager.getZNodeData("/Spark/Cassandra/cqlInitFilePath");
    }

    /**
     * Method initialize cassandra db with queries in cql file
     */
    @PostConstruct
    private void cassCqlInit() {
        String cqlData = "";
        try {
            cqlData = IOUtils.toString(this.getClass().getResourceAsStream(cqlFilePath), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] arrayOfQueries = cqlData.split(";");
        List<String> listOfQueries = Arrays.asList(arrayOfQueries);
        listOfQueries.forEach(query -> {
            query = query.replaceAll("\n", "");
            cassandraRepository.executeQuery(query);
        });
    }
}
