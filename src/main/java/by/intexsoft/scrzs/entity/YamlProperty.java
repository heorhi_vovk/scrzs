package by.intexsoft.scrzs.entity;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class for yaml file mapping
 */
@Data
public class YamlProperty {

    private Map<String, String> rabbit;

    private Map<String, String> zookeeper;

    private Map<String, String> configMap;

    public Map<String, String> getConfigMap() {
        configMap = new LinkedHashMap(rabbit);
        configMap.putAll(zookeeper);
        return configMap;
    }
}
