package by.intexsoft.scrzs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * User entity class
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private Long userid;
    private String username;
    private String firstname;
    private String lastname;
    private int age;
    private String phonenumber;
}
