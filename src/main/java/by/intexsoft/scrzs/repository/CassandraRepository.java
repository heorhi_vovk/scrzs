package by.intexsoft.scrzs.repository;

import by.intexsoft.scrzs.entity.User;
import com.datastax.driver.core.Session;
import com.datastax.spark.connector.cql.CassandraConnector;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class CassandraRepository {

    private final CassandraConnector cassandraConnector;

    @Lazy
    @Autowired
    public CassandraRepository(CassandraConnector cassandraConnector) {
        this.cassandraConnector = cassandraConnector;
    }

    /**
     * Method execute query to delete user by Id
     *
     * @param userId user id
     */
    public void deleteUserById(Long userId) {
        String query = "DELETE FROM mykeyspace.user WHERE userid = " + userId;
        Session session = cassandraConnector.openSession();
        session.execute(query);
        log.info("Delete user with id - {}", userId);
    }

    /**
     * Method execute query to update user by it id
     *
     * @param user new updated user
     */
    public void updateUser(User user) {
        String updateQuery = "UPDATE mykeyspace.user SET username='" + user.getUsername() +
                "', firstname='" + user.getFirstname() +
                "', lastname='" + user.getLastname() +
                "', age=" + user.getAge() +
                ", phonenumber='" + user.getPhonenumber() +
                "' WHERE userid=" + user.getUserid();
        Session session = cassandraConnector.openSession();
        session.execute(updateQuery);
        log.info("Update user with id - {}", user.getUserid());
    }

    /**
     * Method execute some sql query
     *
     * @param query query in string
     */
    public void executeQuery(String query) {
        Session session = cassandraConnector.openSession();
        session.execute(query);
    }
}
