package by.intexsoft.scrzs;

import by.intexsoft.scrzs.config.AppConfig;
import by.intexsoft.scrzs.entity.User;
import by.intexsoft.scrzs.service.RabbitService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Class for application running
 */
public class Runner {

    /**
     * Application entry point
     *
     * @param args cmd input arguments
     */
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        RabbitService rabbit = context.getBean(RabbitService.class);
        User user = new User();
        user.setUserid(99050L);
        rabbit.sendToExchange(user, "read");
//        try {
//            Thread.sleep(10000);
//        rabbit.sendToExchange(88096678L, "delete");
//        Thread.sleep(10000);
//        rabbit.sendToExchange("{ \n" +
//                "\t\"userid\": 1248712,\n" +
//                "    \"username\": \"userFromJsonMessage\",\n" +
//                "\t\"firstname\": \"Jameqwwdas\",\n" +
//                "\t\"lastname\": \"Oflawdawdin\",\n" +
//                "\t\"age\": 72,\n" +
//                "\t\"phonenumber\": \"+4819213213\"\n" +
//                "}", "create");
//        Thread.sleep(10000);
//        User user = new User(88066666L, "userToUpdate2", "firstn2ameeee", "lastna2meeee", 323, "+9999929999");
//        rabbit.sendToExchange(user, "update");
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }
}
