package by.intexsoft.scrzs.config;

import by.intexsoft.scrzs.service.ZKInitService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Application configuration class
 */
@Configuration
@Import({PropertiesConfig.class, ZooConfig.class, ZKInitService.class, SparkCassandraConfig.class, RabbitConfig.class})
@ComponentScan(basePackages = "by.intexsoft.scrzs")
public class AppConfig {

    /**
     * Been of ObjectMapper wich provides functionality for reading and writing JSON
     *
     * @return objectMapper
     */
    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
