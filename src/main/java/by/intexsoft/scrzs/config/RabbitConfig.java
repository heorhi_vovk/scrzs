package by.intexsoft.scrzs.config;

import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * RabbitMQ configuration class
 */

@Configuration
@PropertySource(value = "classpath:app.properties")
public class RabbitConfig {

    @Value("${rabbitmq.host}")
    private String hostName;

    @Value("${rabbitmq.messagesExchange}")
    private String exchange;

    /**
     * Setting connection from RabbitMQ
     *
     * @return ConnectionFactory
     */
    @Bean
    public ConnectionFactory connectionFactory() {
        return new CachingConnectionFactory(hostName);
    }

    /**
     * Messages producer
     *
     * @return RabbitTemplate entity
     */
    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        rabbitTemplate.setExchange(exchange);
        return rabbitTemplate;
    }

    /**
     * Messages consumer
     *
     * @return rabbit listener conteiner factory
     */
    @Bean(name = "rabbitListenerContainerFactory")
    public SimpleRabbitListenerContainerFactory listenerFactory() {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setMessageConverter(jsonMessageConverter());
        return factory;
    }

    /**
     * Json message converter for rabbitMQ
     *
     * @return json message converter entity
     */
    @Bean
    public MessageConverter jsonMessageConverter() {
        return new JsonMessageConverter();
    }

}
