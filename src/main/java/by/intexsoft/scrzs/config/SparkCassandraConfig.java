package by.intexsoft.scrzs.config;

import by.intexsoft.scrzs.service.ZKInitService;
import by.intexsoft.scrzs.service.ZKManager;
import com.datastax.spark.connector.cql.CassandraConnector;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;

/**
 * Spark Cassandra configuration class
 */
@Order(-1)
@Configuration
public class SparkCassandraConfig {
    private final String host;
    private final String deployMode;
    private final String appName;
    private final String maxCores;
    private final String masterUrl;
    private final String contextJar;

    @Autowired
    public SparkCassandraConfig(ZKManager zkManager, ZKInitService zkInitService) {
        host = zkManager.getZNodeData("/Spark/Cassandra/connection_host");
        deployMode = zkManager.getZNodeData("/Spark/submit_deployMode");
        appName = zkManager.getZNodeData("/Spark/app_name");
        maxCores = zkManager.getZNodeData("/Spark/cores_max");
        masterUrl = zkManager.getZNodeData("/Spark/master_url");
        contextJar = zkManager.getZNodeData("/Spark/context_jar");
    }

    /**
     * Create and config spark java context for cassandra service
     *
     * @return java spark context
     */
    @Lazy
    @Bean
    public JavaSparkContext javaSparkContext() {
        SparkConf conf = new SparkConf(true)
                .set("spark.cassandra.connection.host", host)
                .set("spark.submit.deployMode", deployMode);
        conf.setAppName(appName);
        conf.set("spark.cores.max", maxCores);
        conf.setMaster(masterUrl);
        JavaSparkContext javaSparkContext = new JavaSparkContext(conf);
        javaSparkContext.addJar(contextJar);
        return javaSparkContext;
    }

    /**
     * Create cassandra connector for session creation
     *
     * @return cassandra connector
     */
    @Lazy
    @Bean
    public CassandraConnector cassandraConnector() {
        return CassandraConnector.apply(javaSparkContext().getConf());
    }
}
