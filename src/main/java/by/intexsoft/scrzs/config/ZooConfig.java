package by.intexsoft.scrzs.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.RetryNTimes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 * Zookeeper config class
 */
@Slf4j
@Configuration
@Order(-2)
public class ZooConfig {

    @Value("${zookeeper.host}")
    private String zookeeperHost;

    @Value("${zookeeper.nameSpace}")
    private String nameSpace;

    @Value("${zookeeper.sleepMsBetweenConnectRetries}")
    private String sleepMsBetweenRetries;

    @Value("${zookeeper.maxConnectRetries}")
    private String maxRetries;

    /**
     * Method config zookeeper connection with curator framework
     *
     * @return started zookeeper curator framework client
     */
    @Bean
    public CuratorFramework createConnection() {
        RetryPolicy retryPolicy = new RetryNTimes(Integer.parseInt(maxRetries), Integer.parseInt(sleepMsBetweenRetries));
        CuratorFramework client = CuratorFrameworkFactory.builder()
                .namespace(nameSpace)
                .connectString(zookeeperHost)
                .retryPolicy(retryPolicy)
                .build();
        client.start();
        return client;
    }
}
