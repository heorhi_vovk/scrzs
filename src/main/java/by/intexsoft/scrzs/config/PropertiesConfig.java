package by.intexsoft.scrzs.config;

import by.intexsoft.scrzs.entity.YamlProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.yaml.snakeyaml.Yaml;

import javax.annotation.PostConstruct;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Properties config class
 */
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@Configuration
public class PropertiesConfig {

    /**
     * Method generate app.properties file by get config from app.j2 and app.yaml files
     */
    @PostConstruct
    private void propertiesSetup() {
        log.info("Start properties creating with app.j2 and app.yaml files");
        Properties prop = new Properties();
        Yaml yaml = new Yaml();
        InputStream in = PropertiesConfig.class.getClassLoader().getResourceAsStream("app.j2");
        InputStream inputStream = PropertiesConfig.class.getClassLoader().getResourceAsStream("app.yaml");
        try {
            prop.load(in);
            YamlProperty configMaps = yaml.load(inputStream);
            prop.stringPropertyNames().forEach(propName ->
                    prop.setProperty(propName, configMaps.getConfigMap().get(prop.getProperty(propName)))
            );
            prop.store(new FileOutputStream("./src/main/resources/app.properties"), null);
            in.close();
            inputStream.close();
        } catch (IOException e) {
            log.error("Cant create app.properties file, get exception {}", e.getMessage());
        }
    }
}
