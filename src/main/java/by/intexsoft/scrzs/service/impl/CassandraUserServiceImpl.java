package by.intexsoft.scrzs.service.impl;

import by.intexsoft.scrzs.entity.User;
import by.intexsoft.scrzs.repository.CassandraRepository;
import by.intexsoft.scrzs.service.CassandraUserService;
import by.intexsoft.scrzs.service.ZKManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.*;

/**
 * Cassandra user service with crud operations
 */
@Slf4j
@Service
@Order(0)
public class CassandraUserServiceImpl implements CassandraUserService {

    private final JavaSparkContext javaSparkContext;
    private final CassandraRepository cassandraRepository;
    private final String keyspace;

    @Lazy
    @Autowired
    public CassandraUserServiceImpl(JavaSparkContext javaSparkContext, ZKManager zkManager, CassandraRepository cassandraRepository) {
        this.javaSparkContext = javaSparkContext;
        this.cassandraRepository = cassandraRepository;
        keyspace = zkManager.getZNodeData("/Spark/Cassandra/keyspace");
    }

    @Override
    public User getUserById(Long userId) {
        log.info("Get user by id - {}", userId);
        JavaRDD<User> usersById = javaFunctions(javaSparkContext).cassandraTable(keyspace,
                "user", mapRowTo(User.class)).where("userId=?", userId);
        if (usersById.isEmpty()) {
            System.out.println("404 user with id = " + userId + " not found ");
            return new User();
        } else {
            System.out.println("\nUser firstname is - " + usersById.first().toString() + "\n");
            return usersById.first();
        }
    }

    @Override
    public void saveUser(User user) {
        List<User> userList = new ArrayList<>();
        userList.add(user);
        log.info("Save user with id {} in base", user.getUserid());
        JavaRDD<User> userInfoJavaRDD = javaSparkContext.parallelize(userList);
        javaFunctions(userInfoJavaRDD).writerBuilder(keyspace, "user", mapToRow(User.class)).saveToCassandra();
    }

    @Override
    public void deleteUserById(Long userId) {
        cassandraRepository.deleteUserById(userId);
    }

    @Override
    public void updateUser(User user) {
        cassandraRepository.updateUser(user);
    }
}
