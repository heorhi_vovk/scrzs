package by.intexsoft.scrzs.service.impl;

import by.intexsoft.scrzs.service.ZKManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Zookeeper manager class with connection creation and simple operation with znodes
 */
@Slf4j
@Service
public class ZKManagerImpl implements ZKManager {

    private final CuratorFramework client;

    @Autowired
    public ZKManagerImpl(@Qualifier("createConnection") CuratorFramework client) {
        this.client = client;
    }

    @Override
    public boolean create(String path, byte[] data) {
        return false;
    }

    @Override
    public String getZNodeData(String path) {
        try {
            return new String(client.getData().forPath(path));
        } catch (Exception e) {
            log.error("Cant get zookeeper node, get exception {}", e.getMessage());
            return "";
        }
    }
}
