package by.intexsoft.scrzs.service;

import by.intexsoft.scrzs.entity.User;

/**
 * Cassandra user service interface
 */
public interface CassandraUserService {

    /**
     * Method get user from cassandra by user id
     *
     * @param userId id of needed user
     */
    User getUserById(Long userId);

    /**
     * Mthod save user instance to cassandra
     *
     * @param user instance for saving
     */
    void saveUser(User user);

    /**
     * Method delete user instance row on cassandra by user id
     *
     * @param userId user id
     */
    void deleteUserById(Long userId);

    /**
     * Method update user instance on cassandra
     *
     * @param user updated user instance
     */
    void updateUser(User user);
}
