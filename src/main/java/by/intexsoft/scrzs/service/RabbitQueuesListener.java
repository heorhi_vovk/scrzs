package by.intexsoft.scrzs.service;

import by.intexsoft.scrzs.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * RabbitMQ queue listeners class
 */
@Slf4j
@Service
@EnableRabbit
public class RabbitQueuesListener {
    private final CassandraUserService cassandraUserServiceImpl;

    @Lazy
    @Autowired
    public RabbitQueuesListener(CassandraUserService cassandraUserServiceImpl) {
        this.cassandraUserServiceImpl = cassandraUserServiceImpl;
    }

    /**
     * Read queue listener method
     *
     * @param messageFromQueue message form queue
     */
    @RabbitListener(queues = "ReadQueue")
    public void listenReadQueue(User messageFromQueue) {
        log.info("Getting message {} from read queue", messageFromQueue);
        cassandraUserServiceImpl.getUserById(messageFromQueue.getUserid());

    }

    /**
     * Create queue listener method
     *
     * @param messageFromQueue message form queue
     */
    @RabbitListener(queues = "CreateQueue")
    public void listenCreateQueue(User messageFromQueue) {
        log.info("Getting message from create queue {}", messageFromQueue);
        cassandraUserServiceImpl.saveUser(messageFromQueue);

    }

    /**
     * Delete queue listener method
     *
     * @param messageFromQueue message from queue
     */
    @RabbitListener(queues = "DeleteQueue")
    public void listenDeleteQueue(User messageFromQueue) {
        log.info("Getting message {} from delete queue", messageFromQueue);
        cassandraUserServiceImpl.deleteUserById(messageFromQueue.getUserid());

    }

    /**
     * Update method listener method
     *
     * @param messageFromQueue message from queue
     */
    @RabbitListener(queues = "UpdateQueue")
    public void listenUpdateQueue(User messageFromQueue) {
        log.info("Getting message {} from update queue", messageFromQueue);
        cassandraUserServiceImpl.updateUser(messageFromQueue);
    }
}
