package by.intexsoft.scrzs.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * Zookeeper initialization service
 */
@Slf4j
@Service
@Order(-2)
public class ZKInitService {

    private final CuratorFramework client;
    private Map<String, byte[]> zkData = new HashMap<>();

    @Autowired
    public ZKInitService(@Qualifier("createConnection") CuratorFramework client) {
        this.client = client;
    }

    /**
     * Method download init data to zookeeper
     */
    @PostConstruct
    public void zookeeperSetup() {
        log.info("Initialize zookeeper nodes");
        zkData.put("Cassandra/connection_host", "10.0.75.1".getBytes());
        zkData.put("Cassandra/connection_port", "9042".getBytes());
        zkData.put("Cassandra/keyspace", "mykeyspace".getBytes());
        zkData.put("Cassandra/cqlInitFilePath", "/cql/cassCql.cql".getBytes());
        zkData.put("submit_deployMode", "client".getBytes());
        zkData.put("app_name", "mysparkapp".getBytes());
        zkData.put("cores_max", "1".getBytes());
        zkData.put("master_url", "spark://10.0.75.1:7077".getBytes());
        zkData.put("context_jar", "./target/scrzs-1.0-SNAPSHOT.jar".getBytes());
//        zkData.put("context_jar", "/usr/share/myservice/scrzs.jar".getBytes());
        createGroup("Spark", "spark".getBytes());
        createGroup("Spark/Cassandra", "cass".getBytes());
        zkData.forEach((path, data) -> createMemberGroup("Spark", path, data));
    }

    /**
     * Zookeeper group creating
     *
     * @param groupName group name
     * @param data      group data
     */
    private void createGroup(String groupName, byte[] data) {
        try {
            client.create().withMode(CreateMode.PERSISTENT).withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE).forPath("/" + groupName, data);
        } catch (Exception e) {
            log.error("Cant create zookeeper group connection, get exception {}", e.getMessage());
        }
    }

    /**
     * Method create znode on zk server
     *
     * @param groupName  group name
     * @param memberName member name
     * @param data       member data
     */
    private void createMemberGroup(String groupName, String memberName, byte[] data) {
        StringBuilder path = new StringBuilder("/" + groupName + "/" + memberName);
        try {
            client.create().withMode(CreateMode.EPHEMERAL).withACL(ZooDefs.Ids.OPEN_ACL_UNSAFE).forPath(path.toString(), data);
        } catch (Exception e) {
            log.error("Cant create zookeeper group connection, get exception {}", e.getMessage());
        }
    }
}
