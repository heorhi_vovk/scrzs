package by.intexsoft.scrzs.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * RabbiMQ messages service
 */
@Slf4j
@Service
public class RabbitService {

    @Value("${rabbitmq.messagesExchange}")
    private String exchange;

    private RabbitTemplate rabbitTemplate;

    @Autowired
    public RabbitService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    /**
     * Method send message to exchange with routing key
     *
     * @param routingKey routing key for exchanger
     * @param message    message
     */
    public void sendToExchange(Object message, String routingKey) {
        log.info("Sending message {} to exchange, with routing key {}", message, routingKey);
        rabbitTemplate.convertAndSend(exchange, routingKey, message);
    }
}
