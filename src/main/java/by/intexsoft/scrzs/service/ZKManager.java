package by.intexsoft.scrzs.service;

import org.springframework.stereotype.Service;

/**
 * Zookeeper manager interface
 */
@Service
public interface ZKManager {

    /**
     * Method create znode on zk server
     *
     * @param path node tree path
     * @param data data to node
     */
    boolean create(String path, byte[] data);

    /**
     * Method get znode data from zk server
     *
     * @param path tree path to znode
     * @return znode data
     */
    String getZNodeData(String path);
}
