FROM openjdk:8-jre
MAINTAINER Heorhi Vovk <someEmail@adress.com>

ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/myservice/scrzs.jar"]

ARG JAR_FILE
ADD target/scrzs-1.0-SNAPSHOT.jar /usr/share/myservice/scrzs.jar